package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

import com.epam.rd.java.basic.task7.db.entity.*;

import static java.sql.DriverManager.getConnection;


public class DBManager {

	private static DBManager instance;
//	private static Connection connection;
	private static final String DATABASE_URL;
	private static final Properties properties = new Properties();

	static {
		try{
			properties.load(new FileReader("app.properties"));
		}
		catch ( IOException e ) {
			e.printStackTrace();
		}
		DATABASE_URL = (String) properties.get("connection.url");
	}

	public static synchronized DBManager getInstance() {

		if ( instance == null ){
			try {
				instance = new DBManager();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> resultList = new ArrayList<>();
		String sql = "SELECT u.id, u.login FROM users u ORDER BY u.id";

		try ( Connection connection = getConnection(DATABASE_URL);
			  PreparedStatement statement = connection.prepareStatement(sql)) {

			ResultSet resultSet = statement.executeQuery();

			while ( resultSet.next() ) {
				User user = new User();
				user.setId(resultSet.getInt("ID"));
				user.setLogin(resultSet.getString("login"));
				resultList.add(user);
			}

		} catch (SQLException e) {
			throw new DBException("Error while finding all users", e);
		}
		return resultList;
	}

	public boolean insertUser(User user) throws DBException {
		int rowsUpdate;
		String sql = "INSERT INTO users(login) VALUES (?)";

		try(Connection connection = getConnection(DATABASE_URL);
			PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

			ps.setString(1, user.getLogin());
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			while ( rs.next() ) {
				user.setId(rs.getInt(1));
			}
			rowsUpdate = 1;
		}
		catch (SQLException e) {
			throw new DBException("Error while inserting user", e);
		}
		return rowsUpdate == 1;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users.length == 0) return false;

		int rowsUpdate;
		StringJoiner sj = new StringJoiner(",");

		for ( User user : users ) {
			sj.add(String.valueOf(user.getId()));
		}
		String sql = String.format("DELETE FROM users WHERE id in(%s)", sj);

		try(Connection connection = getConnection(DATABASE_URL);
			PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.executeUpdate();
			rowsUpdate = 1;
		}
		catch (SQLException e) {
			throw new DBException("Error while deleting users", e);
		}
		return rowsUpdate == 1;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		String sql = "SELECT u.id, u.login FROM users u WHERE login=? ORDER BY id";

		try (Connection connection = getConnection(DATABASE_URL);
			 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();

			while( resultSet.next() ) {
				user = User.createUser(login);
				user.setId(resultSet.getInt(1));
				break;
			}

		} catch (SQLException e) {
			throw new DBException("Error while getting user", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		String sql = "SELECT id, name FROM teams WHERE name=? ORDER BY id";
		try (Connection connection = getConnection(DATABASE_URL);
			 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();

			while( resultSet.next() ) {
				team = Team.createTeam(name);
				team.setId(resultSet.getInt(1));
				break;
			}

		} catch (SQLException e) {
			throw new DBException("Error while getting team", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> resultList = new ArrayList<>();
		String sql = "SELECT t.id, t.name FROM teams t ORDER BY t.id";

		try (Connection connection = getConnection(DATABASE_URL);
			 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("ID"));
				team.setName(rs.getString("name"));
				resultList.add(team);
			}
		}
		catch (SQLException e) {
			throw new DBException("Error while finding all teams", e);
		}
		return resultList;
	}

	public boolean insertTeam(Team team) throws DBException {
		int rowsUpdate;
		String sql = "INSERT INTO teams( name) VALUES (?)";

		try (Connection connection = getConnection(DATABASE_URL);
		     PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

			preparedStatement.setString(1, team.getName());
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			while ( rs.next() ) {
				team.setId(rs.getInt(1));
			}
			rowsUpdate = 1;
		}
		catch (SQLException e) {
			throw new DBException("Error while inserting team", e);
		}
		return rowsUpdate == 1;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		if (user == null) return false;
		for (Team t : teams) {
			if (t == null) return false;
		}

		int rowsUpdate;
		String sql = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
		Connection connection = null;
		PreparedStatement preparedStatement;
		try {
			connection = getConnection(DATABASE_URL);
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			for ( Team team : teams ) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.executeUpdate();
			}
			connection.commit();
			rowsUpdate = 1;
		}
		catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Error while setting teams for users", e);
		}
		finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowsUpdate == 1;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> resultList = new ArrayList<>();
		List<Integer> teamsId = new ArrayList<>();
		String sql = "SELECT team_id FROM users_teams WHERE user_id=?";

		try(Connection connection = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setInt(1, user.getId());
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				teamsId.add(rs.getInt(1));
			}
		}
		catch (SQLException e) {
			throw new DBException("Error while finding all teams id's", e);
		}

		try(Connection con = getConnection(DATABASE_URL);
			PreparedStatement ps = con.prepareStatement("SELECT name FROM teams WHERE id=?")){

			for ( int id : teamsId ) {
				ps.setInt(1, id);
				ResultSet resSet = ps.executeQuery();

				while (resSet.next()) {
					Team team = new Team();
					team.setId(id);
					team.setName(resSet.getString(1));
					resultList.add(team);
				}
			}

		}
		catch (SQLException e) {
			throw new DBException("Error while finding all teams for user", e);
		}

		return resultList;

	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null) return false;
		int rowsUpdate;
		String sql = "DELETE FROM teams WHERE name=?";

		try(Connection connection = getConnection(DATABASE_URL);
			PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
				preparedStatement.setString(1, team.getName());
				rowsUpdate = preparedStatement.executeUpdate();
		}
		catch (SQLException e) {
			throw new DBException("Error while deleting team", e);
		}
		return rowsUpdate == 1;
	}

	public boolean updateTeam(Team team) throws DBException {
		if (team == null) return false;
		int rowsUpdate;
		String sql = "UPDATE teams t SET t.name=? WHERE t.id=?";

		try(Connection connection = DriverManager.getConnection(DATABASE_URL);
			PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			preparedStatement.executeUpdate();
			rowsUpdate = 1;
		}
		catch (SQLException e) {
			throw new DBException("Error while deleting team", e);
		}
		return rowsUpdate == 1;
	}

}
